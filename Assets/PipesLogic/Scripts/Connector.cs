using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Connector : MonoBehaviour
{
    public GameObject Lid;
    public GameObject DownPipe;
    public GameObject LeftPoint;
    public GameObject RightPoint;
    [HideInInspector]
    public Recipent Recipent;
    public ConnectorType ConnectorType;

    public bool isFull;

    public List<Connector> connectionRelation = new List<Connector>(); 
    public List<Pipe> pipesRelation = new List<Pipe>(); 

    public bool lidActive=true;
    private bool hoverMouse;
    private void Start()
    {
        Recipent = GetComponentInParent<Recipent>();
        if(ConnectorType==ConnectorType.top)
            Lid.SetActive(false);

        
        GameManager.OnStartGame+=OpenLid;

    }

    // private void OpenLidEvent()
    // {
    //     if(ConnectorType!= ConnectorType.top)
    //     {
            
    //     }
    // }

    
    private void OnMouseDown()
    {
        if(!isFull)
        {
            GameManager.Instance.mouseLogic.Connect(this);
        }
        
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Mouse1)&&ConnectorType == ConnectorType.down&&hoverMouse)
        {
            lidActive=!lidActive;
            // Lid.SetActive(lidActive);
            DownPipe.SetActive(!lidActive);
        }
    }

    private void OnMouseEnter()
    {
        hoverMouse=true;
    }

    private void OnMouseExit()
    {
        hoverMouse=false;
    }

    public void RestartConnector()
    {
        if(ConnectorType==ConnectorType.top)
            Lid.SetActive(false);
        else
        {
            Lid.SetActive(true);
        }

        DownPipe.SetActive(false);
            
        lidActive=true;
        isFull=false;
        connectionRelation.Clear();
        pipesRelation.Clear();
    }

    public void OpenLid()
    {
        if(!lidActive)
        {
            // if(ConnectorType!= ConnectorType.top)
            // Lid.SetActive(false);
            // else
            Lid.SetActive(false);
        }
        
    }

    public bool CheckConnection(ConnectorType conT)
    {
        int ct= (int)conT;
        switch(ConnectorType)
        {
            case ConnectorType.top:
            {
                if(ct!=0)
                {
                    return true;
                }
                else
                    return false;
            }

            case ConnectorType.down:
            {
                if(ct==0)
                {
                    return true;
                }
                else
                    return false;
            }

            case ConnectorType.left:
            {
                if(ct==0||ct==3)
                {
                    return true;
                }   
                else
                    return false;
            }

            case ConnectorType.right:
            {
                if(ct==0||ct==2)
                {
                    return true;
                }
                else 
                    return false;
            }
            
        }
        Debug.Log("conexion desconocida");
        return false;

    }
}


public enum ConnectorType
{
    top,
    down,
    left,
    right
}
