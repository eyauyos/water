using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Recipent : MonoBehaviour
{
    public bool isFirstRecipent;
    public bool isWinRecipent;
    public int winCount=0;
    public GameObject RecipentUI;
    public Slider sliderParticle;
    public Text percent;
    public Button exitPanel;

    public WaterRecipentController wrc;
    private List<Connector> connectors;

    private bool hoverMouse;

    private void Start()
    {
        connectors= new List<Connector>(FindObjectsOfType<Connector>());
        percent.text="0%";
        
        sliderParticle.minValue=0;
        sliderParticle.maxValue=wrc.particleCount;
        sliderParticle.onValueChanged.AddListener(WaterAmount);

        exitPanel.onClick.AddListener(()=>
        {
            RecipentUI.SetActive(false);
        });

        GameManager.OnStartGame += StartGameEvent;
    }

    private void StartGameEvent()
    {
        RecipentUI.SetActive(false);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Mouse1)&&hoverMouse)
        {
            RecipentUI.SetActive(true);
        }
    }

    private void OnMouseEnter()
    {
        hoverMouse=true;
    }

    private void OnMouseExit()
    {
        hoverMouse=false;
    }

    private void WaterAmount(float value)
    {
        wrc.EnableWater((int)value);
        float currentPercent = (value/sliderParticle.maxValue)*100;
        percent.text = (int)currentPercent+"%";
    }

    public void ResetConnectors()
    {
        foreach(var c in connectors)
        {
            c.RestartConnector();
        }
    }

    public void ResetRecipent()
    {
        
        ResetConnectors();
        wrc.ResetWater();
        sliderParticle.value=sliderParticle.minValue;
        RecipentUI.SetActive(false);
        winCount=0;
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Metaball_liquid"))//&&!GameManager.Instance.enableConfig)
        {
            winCount++;
            if(winCount>=GameManager.Instance.m_WinParticleCount&&isWinRecipent)
            {
                GameManager.OnWinEvent();
                return;
            }

            if(winCount>=GameManager.Instance.m_LoseParticleCount)
            {
                GameManager.OnLoseEvent();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag("Metaball_liquid"))//&&!GameManager.Instance.enableConfig)
        {
            winCount--;
        }
    } 
}
