using System.Collections.Generic;
using UnityEngine;
using System;

public class Pipe : MonoBehaviour
{
    public GameObject[] Wall;
    public float wallThickness;
    public Connector A;
    public Connector B;
    
    public List<Line> Lines = new List<Line>();

    public static Vector3 CenterPipe(Connector conA, Connector conB)
    {
        Vector3 direction = conA.RightPoint.transform.position-conB.LeftPoint.transform.position;
        Vector3 helpVector= conB.LeftPoint.transform.position+direction.normalized*direction.magnitude/2;
        return helpVector;
    }

    public void SetPipe(Connector conA, Connector conB)
    {
        if(conA.ConnectorType==ConnectorType.right&&conB.ConnectorType==ConnectorType.top)
        {
            // Vector3 direction = conA.RightPoint.transform.position-conB.LeftPoint.transform.position;
            // Wall[0].transform.localScale=new Vector3(wallThickness,direction.magnitude,1f);
            // Vector3 helpVector= conB.LeftPoint.transform.position+direction.normalized*direction.magnitude/2;
            // Wall[0].transform.position=helpVector;
            // Wall[0].transform.up=direction;

            Lines.Add(SetWall(conA.RightPoint.transform.position,conB.LeftPoint.transform.position,0));
            
            

            
            // Vector3 direction1 = conA.LeftPoint.transform.position-conB.RightPoint.transform.position;
            // Wall[1].transform.localScale=new Vector3(wallThickness,direction1.magnitude,1f);
            // Vector3 helpVector1= conB.RightPoint.transform.position+direction1.normalized*direction1.magnitude/2;
            // Wall[1].transform.position=helpVector1;
            // Wall[1].transform.up=direction1;

            Lines.Add(SetWall(conA.LeftPoint.transform.position,conB.RightPoint.transform.position,1));
        }
        else
        {
            // Vector3 direction = conA.LeftPoint.transform.position-conB.LeftPoint.transform.position;
            // Wall[0].transform.localScale=new Vector3(wallThickness,direction.magnitude,1f);
            // Vector3 helpVector= conB.LeftPoint.transform.position+direction.normalized*direction.magnitude/2;
            // Wall[0].transform.position=helpVector;
            // Wall[0].transform.up=direction;

            Lines.Add(SetWall(conA.LeftPoint.transform.position,conB.LeftPoint.transform.position,0));

            // Vector3 direction1 = conA.RightPoint.transform.position-conB.RightPoint.transform.position;
            // Wall[1].transform.localScale=new Vector3(wallThickness,direction1.magnitude,1f);
            // Vector3 helpVector1= conB.RightPoint.transform.position+direction1.normalized*direction1.magnitude/2;
            // Wall[1].transform.position=helpVector1;
            // Wall[1].transform.up=direction1;

            Lines.Add(SetWall(conA.RightPoint.transform.position,conB.RightPoint.transform.position,1));
        }

        
    }

    public Line SetWall(Vector3 start, Vector3 end, int wallId)
    {
        Vector3 direction = end-start;
        Wall[wallId].transform.localScale=new Vector3(wallThickness,direction.magnitude,1f);
        Vector3 helpVector= start+direction.normalized*direction.magnitude/2;
        Wall[wallId].transform.position=helpVector;
        Wall[wallId].transform.up=direction;

        

        return new Line(start,end,direction,wallId);
    }

    

}

[Serializable]
public class Line
{
    public Vector3 start;
    public Vector3 end;
    public Vector3 vector;
    public int wallId;
    public bool intersection;

    public Line(Vector3 start, Vector3 end, Vector3 vector, int wallId)
    {
        this.start = start;
        this.end = end;
        this.vector = vector;
        this.wallId=wallId;
    }
}
