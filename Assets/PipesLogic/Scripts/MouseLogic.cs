using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLogic : MonoBehaviour
{
    public Pipe _refPipePrefab;
    
    [HideInInspector]public Connector currentConectorA;
    [HideInInspector]public Connector currentConectorB;
    private Recipent currentRecipent;
    private ConnectorType currentCT;

    public delegate void SetPipe(Connector conA, Connector conB);
    public event SetPipe OnSetPipe;

    // Start is called before the first frame update
    void Start()
    {
        OnSetPipe+=GeneratePipe;
    }

    public void GeneratePipe(Connector conA,Connector conB)
    {

        if(!conB.isFull&&!conA.isFull)
        {
            GameObject pipeGO= Instantiate(_refPipePrefab.gameObject,Pipe.CenterPipe(conA,conB),Quaternion.identity);
            pipeGO.SetActive(true);
            pipeGO.GetComponent<Pipe>().SetPipe(conA,conB);

            conA.pipesRelation.Add(pipeGO.GetComponent<Pipe>());
            conB.pipesRelation.Add(pipeGO.GetComponent<Pipe>());
        
            conA.lidActive=false;
            conB.lidActive=false;
        }

        if(conA.connectionRelation.Count<2)
        {
            conA.connectionRelation.Add(conB);
            if(conA.connectionRelation.Count==2)
            {
                conA.isFull=true;
            }
        }
        
            
        if(conB.connectionRelation.Count<2)
        {
            conB.connectionRelation.Add(conA);
            if(conB.connectionRelation.Count==2)
            {
                conB.isFull=true;
            }   
        }
            
        
            
        

        if(conA.isFull)
        {
            //for(int i=0; i<2;i++)
            //{
                Debug.Log(conA+" está lleno");
                Vector2 intersection;
                // Pipe.AreLinesIntersecting(out intersection,conA.pipesRelation[0].Lines[0],conA.pipesRelation[1].Lines[0], true);
                // Debug.Log(intersection);
                // Pipe.AreLinesIntersecting(out intersection,conA.pipesRelation[0].Lines[0],conA.pipesRelation[1].Lines[1], true);
                // Debug.Log(intersection);
                // Pipe.AreLinesIntersecting(out intersection,conA.pipesRelation[0].Lines[1],conA.pipesRelation[1].Lines[1], true);
                // Debug.Log(intersection);
                // Pipe.AreLinesIntersecting(out intersection,conA.pipesRelation[0].Lines[1],conA.pipesRelation[1].Lines[0], true);
                // Debug.Log(intersection);

                // LineUtil.IntersectLineSegments2D(conA.pipesRelation[0].Lines[0],conA.pipesRelation[1].Lines[0],out intersection);
                // Debug.Log(intersection);
                // LineUtil.IntersectLineSegments2D(conA.pipesRelation[0].Lines[0],conA.pipesRelation[1].Lines[1],out intersection);
                // Debug.Log(intersection);
                // LineUtil.IntersectLineSegments2D(conA.pipesRelation[0].Lines[1],conA.pipesRelation[1].Lines[1],out intersection);
                // Debug.Log(intersection);
                // LineUtil.IntersectLineSegments2D(conA.pipesRelation[0].Lines[1],conA.pipesRelation[1].Lines[0],out intersection);
                // Debug.Log(intersection);

                
            //}



            if(LineUtil.IntersectLineSegments2D(conA.pipesRelation[0].Lines[0],conA.pipesRelation[1].Lines[0],out intersection))
                {
                    Debug.Log(intersection);
                    //Instantiate(circle,intersection,Quaternion.identity);
                    SetNewWall(intersection,conA.pipesRelation[0].Lines[0],conA.pipesRelation[0]);
                    SetNewWall(intersection,conA.pipesRelation[1].Lines[0],conA.pipesRelation[1]);
                };
                
                if(LineUtil.IntersectLineSegments2D(conA.pipesRelation[0].Lines[0],conA.pipesRelation[1].Lines[1],out intersection)) 
                {
                    Debug.Log(intersection);
                    //Instantiate(circle,intersection,Quaternion.identity);
                    SetNewWall(intersection,conA.pipesRelation[0].Lines[0],conA.pipesRelation[0]);
                    SetNewWall(intersection,conA.pipesRelation[1].Lines[1],conA.pipesRelation[1]);
                };
                
                if(LineUtil.IntersectLineSegments2D(conA.pipesRelation[0].Lines[1],conA.pipesRelation[1].Lines[1],out intersection))
                {
                    Debug.Log(intersection);
                    //Instantiate(circle,intersection,Quaternion.identity);
                    SetNewWall(intersection,conA.pipesRelation[0].Lines[1],conA.pipesRelation[0]);
                    SetNewWall(intersection,conA.pipesRelation[1].Lines[1],conA.pipesRelation[1]);
                };
                
                if(LineUtil.IntersectLineSegments2D(conA.pipesRelation[0].Lines[1],conA.pipesRelation[1].Lines[0],out intersection))
                {
                    Debug.Log(intersection);
                    //Instantiate(circle,intersection,Quaternion.identity);
                    SetNewWall(intersection,conA.pipesRelation[0].Lines[1],conA.pipesRelation[0]);
                    SetNewWall(intersection,conA.pipesRelation[1].Lines[0],conA.pipesRelation[1]);
                };
        }

        if(conB.isFull)
        {
            Debug.Log(conB+" está lleno");
            
                Vector2 intersection;

                if(LineUtil.IntersectLineSegments2D(conB.pipesRelation[0].Lines[0],conB.pipesRelation[1].Lines[0],out intersection))
                {
                    Debug.Log(intersection);
                    //Instantiate(circle,intersection,Quaternion.identity);
                    SetNewWall(intersection,conB.pipesRelation[0].Lines[0],conB.pipesRelation[0]);
                    SetNewWall(intersection,conB.pipesRelation[1].Lines[0],conB.pipesRelation[1]);
                };
                
                if(LineUtil.IntersectLineSegments2D(conB.pipesRelation[0].Lines[0],conB.pipesRelation[1].Lines[1],out intersection)) 
                {
                    Debug.Log(intersection);
                    //Instantiate(circle,intersection,Quaternion.identity);
                    SetNewWall(intersection,conB.pipesRelation[0].Lines[0],conB.pipesRelation[0]);
                    SetNewWall(intersection,conB.pipesRelation[1].Lines[1],conB.pipesRelation[1]);
                };
                
                if(LineUtil.IntersectLineSegments2D(conB.pipesRelation[0].Lines[1],conB.pipesRelation[1].Lines[1],out intersection))
                {
                    Debug.Log(intersection);
                    //Instantiate(circle,intersection,Quaternion.identity);
                    SetNewWall(intersection,conB.pipesRelation[0].Lines[1],conB.pipesRelation[0]);
                    SetNewWall(intersection,conB.pipesRelation[1].Lines[1],conB.pipesRelation[1]);
                };
                
                if(LineUtil.IntersectLineSegments2D(conB.pipesRelation[0].Lines[1],conB.pipesRelation[1].Lines[0],out intersection))
                {
                    Debug.Log(intersection);
                    //Instantiate(circle,intersection,Quaternion.identity);
                    SetNewWall(intersection,conB.pipesRelation[0].Lines[1],conB.pipesRelation[0]);
                    SetNewWall(intersection,conB.pipesRelation[1].Lines[0],conB.pipesRelation[1]);
                };
            
        }
    }

    private void SetNewWall(Vector3 intersectPoint, Line l1, Pipe pipe)
    {
        if(Vector3.Distance(l1.start,intersectPoint)<Vector3.Distance(l1.end,intersectPoint))
        {
            pipe.SetWall(l1.end,intersectPoint,l1.wallId);
            
        }
        else
        {
            pipe.SetWall(l1.start,intersectPoint,l1.wallId);
        }


    }

    public void Connect(Connector connector)
    {
        //Connector connector = con.GetComponent<Connector>();
        if(currentConectorA==null)
        {
            currentConectorA=connector;
            currentCT = connector.ConnectorType;
            currentRecipent=connector.Recipent;
            return;
        }
        else if(currentConectorB==null)
        {
            if(connector.Recipent!=currentRecipent &&connector.CheckConnection(currentCT))// connector.ConnectorType!=currentCT)
            {
                currentConectorB=connector;
                OnSetPipe?.Invoke(currentConectorA,currentConectorB);
            }
            else
            {
                currentConectorA=null;
                currentConectorB=null;
                //Connect(connector);
            }
            return;
        }
        else
        {
            currentConectorA=null;
            currentConectorB=null;
            Connect(connector);
        }
    }
}
