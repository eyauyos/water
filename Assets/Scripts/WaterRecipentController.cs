using System.Collections.Generic;
using UnityEngine;

public class WaterRecipentController : MonoBehaviour
{
    public int particleCount=70;
    public float randomDelta=0.1f;
    public Transform position;
    public GameObject waterDrop;
    public List<GameObject> WaterDropList;
    // Start is called before the first frame update
    void Start()
    {
        //particleCount=GameManager.Instance.m_ParticleCount;
        InstantiateWater();
    }

    private void InstantiateWater()
    {
        for(int i=0; i<particleCount;i++)
        {
            Vector3 pos = new Vector3(Random.Range(position.position.x,position.position.x+randomDelta),Random.Range(position.position.y,position.position.y+randomDelta),Random.Range(position.position.z,position.position.z+randomDelta));
            WaterDropList.Add(Instantiate(waterDrop,pos,Quaternion.identity));
        }
    }

    private void PositionWater()
    {
        foreach(var w in WaterDropList)
        {
            Vector3 pos = new Vector3(Random.Range(position.position.x,position.position.x+randomDelta),Random.Range(position.position.y,position.position.y+randomDelta),Random.Range(position.position.z,position.position.z+randomDelta));
            w.transform.position=pos;

        }
        

    }

    public void ResetWater()
    {
        PositionWater();
        DisableWater();

    }

    public void EnableWater(int count)
    {
        int acc=0;
        if(count>particleCount)
        {
            count=particleCount;
        }
        foreach(var w in WaterDropList)
        {
            acc++;
            
            if(acc<=count)
            {
                w.SetActive(true);
            }
            else
            {
                w.SetActive(false);
            }
        }
    }

    public void DisableWater()
    {
        foreach(var w in WaterDropList)
        {
            w.SetActive(false);
        }
    }

}
