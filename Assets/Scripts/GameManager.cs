using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Water2D;
public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public MouseLogic mouseLogic;
    public Camera m_mainCam;
    public Button m_StopWater;
    public Button m_StartGame;
    public Button m_RestartGame;
    public Button m_RestartBtn;
    public static bool handEnable=true;

    [Header("Game values")]
    public int m_ParticleCount=35;
    public int m_WinParticleCount=35;
    public int m_LoseParticleCount=35;
    public Vector2 directionSpawn;
    public Vector2 positionSpawn;

    public GameObject WaterDrop;
    public List<GameObject> waterObjects = new List<GameObject>();

    public const string UP_CONNECTION="UpConnect";
    public const string DOWN_CONNECTION="DownConnect";

    public delegate void Win();
    public static event Win OnWin;

    public delegate void Lose();
    public static event Lose OnLose;

    public delegate void StartGame();
    public static event StartGame OnStartGame;

    private List<Recipent> recipents;
    private List<Connector> connectors;

    public bool enableConfig=true;


    [Header("Game Status UI")]
    public GameObject PanelStatusGame;
    public GameObject PanelWin;
    public GameObject PanelLose;

    private void Awake() 
    {
        Instance=this;

        OnWin+=WinGameAction;
        OnLose+=LoseGameAction;

        recipents = new List<Recipent>(FindObjectsOfType<Recipent>());
    }

    private void WinGameAction()
    {
        PanelStatusGame.SetActive(true);
        PanelWin.SetActive(true);
    }

    private void LoseGameAction()
    {
        PanelStatusGame.SetActive(true);
        PanelLose.SetActive(true);
    }

    public static void OnWinEvent()
    {
        OnWin?.Invoke();
    }

    public static void OnLoseEvent()
    {
        OnLose?.Invoke();
    }

    private void SetDefaultValues()
    {
        Pipe[] pipearr=FindObjectsOfType<Pipe>();
        List<Pipe> pipeList= new List<Pipe>(pipearr);
        foreach(var p in pipeList)
        {
            Destroy(p.gameObject);
        }

        foreach(var r in recipents)
        {
            // r.winCount=0;
            // r.ResetConnectors();
            r.ResetRecipent();
        }

        EnableConfig();


    }



    private void EnableConfig()
    {
        connectors= new List<Connector>(FindObjectsOfType<Connector>());
        foreach(var c in connectors)
        {
            c.enabled=true;
            c.isFull=false;
        }

        mouseLogic.gameObject.SetActive(true);
    }

    private void DisableConfig()
    {
        connectors= new List<Connector>(FindObjectsOfType<Connector>());
        foreach(var c in connectors)
        {
            c.enabled=false;
            c.isFull=true;
        }

        mouseLogic.gameObject.SetActive(false);
    }

    private void Start() 
    {
        //Water2D_Spawner.instance.Restore();
        //waterObjects= new List<GameObject>(Water2D_Spawner.instance.WaterDropsObjects);
        m_StartGame.onClick.AddListener(()=>
        {
            // if(!Water2D_Spawner.instance.enabled)
            // {
            //     Water2D_Spawner.instance.enabled=true;
            //     Water2D_Spawner.instance.Restore();
            // }
            // else
            // {
                //Water2D_Spawner.instance.Spawn(m_ParticleCount, positionSpawn,directionSpawn);
                Water2D_Spawner.instance.WaterDropsObjects = new GameObject[m_ParticleCount];
                Water2D_Spawner.instance.WaterDropsObjects[0]=WaterDrop;
                Water2D_Spawner.instance.Initialize();
                Water2D_Spawner.instance.initSpeed =directionSpawn;
                
                Water2D_Spawner.instance.RunMicroSpawn(positionSpawn,m_ParticleCount,new Vector2(Random.Range(-directionSpawn.x,directionSpawn.x),Random.Range(-directionSpawn.y,directionSpawn.y)));
            //}
            m_StartGame.gameObject.SetActive(false);
            m_StopWater.gameObject.SetActive(true);

            OnStartGame?.Invoke();
            DisableConfig();
            enableConfig=false;
                
        });

        m_StopWater.onClick.AddListener(()=>
        {
            Water2D_Spawner.instance.Restore();
            m_StopWater.gameObject.SetActive(false);
            m_StartGame.gameObject.SetActive(true);
        });

        m_RestartGame.onClick.AddListener(()=>
        {
            SetDefaultValues();
            Water2D_Spawner.instance.Restore();
            m_StopWater.gameObject.SetActive(false);
            m_StartGame.gameObject.SetActive(true);
            PanelStatusGame.SetActive(false);
            PanelWin.SetActive(false);
            PanelLose.SetActive(false);
            enableConfig=true;

        });

        m_RestartBtn.onClick.AddListener(()=>
        {
            SetDefaultValues();
            Water2D_Spawner.instance.Restore();
            m_StopWater.gameObject.SetActive(false);
            m_StartGame.gameObject.SetActive(true);
            PanelStatusGame.SetActive(false);
            PanelWin.SetActive(false);
            PanelLose.SetActive(false);
            enableConfig=true;

        });



        
        
    }



}
