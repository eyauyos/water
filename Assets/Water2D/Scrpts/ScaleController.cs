using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScaleController : MonoBehaviour
{
    public Transform scale;
    public Slider sliderScale;
    public GameObject UIScale;
    public Button exitPanel;
    private Vector3 unitScale;
    public float maxScale;
    private bool hoverMouse;


    private void Start()
    {
        unitScale=scale.localScale;
        sliderScale.minValue=1;
        sliderScale.maxValue=maxScale;
        sliderScale.onValueChanged.AddListener(ChangeScale);

        exitPanel.onClick.AddListener(()=>
        {
            UIScale.SetActive(false);
        });
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Mouse1)&&hoverMouse)
        {
            UIScale.SetActive(true);
        }
    }

    private void OnMouseEnter()
    {
        hoverMouse=true;
    }

    private void OnMouseExit()
    {
        hoverMouse=false;
    }

    public void ChangeScale(float value)
    {
        Vector3 currentScale=unitScale*value;

        scale.localScale=currentScale;

    }
}
